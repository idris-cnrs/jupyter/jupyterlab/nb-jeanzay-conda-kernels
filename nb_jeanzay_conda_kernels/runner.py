from __future__ import print_function

import os
import sys

try:
    from shlex import quote
except ImportError:
    from pipes import quote


def exec_in_env(
    conda_prefix, env_name, env_path, modules, module_conda_prefix, *command
):
    # Run the standard conda activation script, and print the
    # resulting environment variables to stdout for reading.
    if conda_prefix and modules:
        sys.exit(
            "Setting conda_prefix and modules are not allowed for runner of "
            "nb_jeanzay_conda_kernels. Exiting..."
        )
    is_current_env = env_path == sys.prefix
    if modules:
        # Sanitize PATH env variable
        current_path = os.environ['PATH'].split(':')
        sanitized_path = []
        for p in current_path:
            if module_conda_prefix in p:
                continue
            sanitized_path.append(p)
        sanitized_path = ':'.join(sanitized_path)
        # Create an environment for program with sanitized PATH
        prog_environ = os.environ.copy()
        prog_environ['PATH'] = sanitized_path
        quoted_command = [quote(c) for c in command]
        if is_current_env:
            os.execvpe(quoted_command[0], quoted_command, prog_environ)
        else:
            ecomm = (
                f"module purge && module load {modules} && module list && "
                f"unset SLURM_EXPORT_ENV && echo CONDA_ENV={env_name} && "
                f"exec {' '.join(quoted_command)}"
            )
            print(f'Kernel will be launched with command {ecomm}')
            ecomm = ['bash', '-c', ecomm]
            os.execvpe(ecomm[0], ecomm, prog_environ)

    if conda_prefix:
        quoted_command = [quote(c) for c in command]
        if is_current_env:
            os.execvp(quoted_command[0], quoted_command)
        else:
            activate = os.path.join(conda_prefix, 'bin', 'activate')
            ecomm = ". '{}' '{}' && echo CONDA_PREFIX=$CONDA_PREFIX && exec {}".format(
                activate, env_path, ' '.join(quoted_command)
            )
            ecomm = ['sh' if 'bsd' in sys.platform else 'bash', '-c', ecomm]
            os.execvp(ecomm[0], ecomm)


if __name__ == '__main__':
    exec_in_env(*(sys.argv[1:]))
