# Initial support for the kernel provider mechanism
# to be introduced in jupyter_client 6.0; see
# https://jupyter-client.readthedocs.io/en/latest/kernel_providers.html

from jupyter_client.manager import KernelManager

from .manager import CondaKernelSpecManager


class CondaKernelProvider(object):
    id = 'conda'

    def __init__(self):
        self.cksm = CondaKernelSpecManager()

    def find_kernels(self):
        for name, data in self.cksm.get_all_specs().items():
            yield name, data['spec']

    def make_manager(self, name):
        return KernelManager(kernel_spec_manager=self.cksm, kernel_name=name)
