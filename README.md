# nb_jeanzay_conda_kernels

This is an extension to the original [nb_conda_kernels](https://github.com/Anaconda-Platform/nb_conda_kernels) project that is
customized to JeanZay platform. In addition to adding kernels in different
conda environments of the userspace, this package is capable of picking up
the kernels in the conda environments of environment modules on the Jean Zay
platform. If there are no conda environments in environment modules or even
if there is no environment modules installed on the system, the behaviour of
the package will fallback to original `nb_conda_kernels`

## Installation

To install package

```shell
pip install git+https://gitlab.com/idris-cnrs/jupyter/jupyterlab/nb-jeanzay-conda-kernels.git@main
```

## Usage and configuration

We recommend to check the [README](https://github.com/Anaconda-Platform/nb_conda_kernels/blob/master/README.md) of the `nb_conda_kernels` project for
usage and configuration details.

To use the package as `kernel_spec_manager_class`, we need to add the following
config to `jupyter_server_config.json`.

```json
{
  "ServerApp": {
    "kernel_spec_manager_class": "nb_jeanzay_conda_kernels.CondaKernelSpecManager"
  }
}
```

This file must be placed in the `sys.prefix/etc/jupyter` for JupyterLab or
notebook to read the configuration. If not, we can pass this configuration _via_
CLI as well

```bash
jupyter-lab --ServerApp.kernel_spec_manager_class=nb_jeanzay_conda_kernels.CondaKernelSpecManager
```

Conda environments activated through environment modules can be sticky. In order
to make sure if sanitize the `PATH` variable properly, a configuration
parameter `module_conda_prefix` is added to the package. By setting this value,
we remove all the paths that have this value from the `PATH` before loading
module. This can be added to the global config as follows:

```json
{
  "CondaKernelSpecManager": {
    "module_conda_prefix": "/opt/conda"
  }
}
```

Rest of the configuration parameters are from the original project.
