[build-system]
requires = ["hatchling", "hatch-vcs"]
build-backend = "hatchling.build"

[project]
name = "nb_jeanzay_conda_kernels"
authors = [
    {name = "Mahendra Paipuri", email = "mahendra.paipuri@cnrs.fr"},
]
description = """Launch Jupyter kernels for any environment module
    that has ipykernel installed on Jean Zay platform"""
readme = "README.md"
requires-python = ">=3.8"
keywords = ["Interactive", "Interpreter", "Shell", "Web", "Jupyter"]
license = {text = "Apache 2.0"}
classifiers = [
    "Framework :: Jupyter",
    "Programming Language :: Python :: 3",
    "License :: OSI Approved :: Apache Software License",
    "Operating System :: MacOS :: MacOS X",
    "Operating System :: POSIX :: Linux",
    "Intended Audience :: Developers",
    "Intended Audience :: System Administrators",
    "Intended Audience :: Science/Research",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
]
dependencies = [
    "cachetools>=5.2.0",
    "jupyter_client>=7.4.8",
]
dynamic = ["version"]

[project.urls]
"Homepage" = "https://gitlab.com/idris-cnrs/jupyter/jupyterlab/nb-jeanzay-conda-kernels.git"

[project.optional-dependencies]
test = [
    "pytest>=3.3",
    "pytest-asyncio",
    "notebook",
    "jupyter_client",
    "requests",
]
lint = [
    "isort",
    "black",
    # Seems like > 2.15, pylint_junit does not produce output
    # https://github.com/rasjani/pylint_junit/issues/3
    "pylint==2.14.5",
    "pylint_junit",
]
cov = [
    "pytest-html",
    "pytest-cov",
]

[tool.hatch.version]
source = "vcs"

[tool.hatch.build.hooks.vcs]
version-file = "nb_jeanzay_conda_kernels/_version.py"

# [tool.hatch.build.targets.wheel.shared-data]
# "nb_jeanzay_conda_kernels/etc/jupyter_server_config.json" = "etc/jupyter/jupyter_server_config.json"

[tool.hatch.build.targets.sdist]
exclude = [
    ".github",
]

[tool.pylint.master]
load-plugins = [
    "pylint_junit"
]
exit-zero = true

[tool.pylint.'REPORTS']
output-format = "junit"

[tool.pylint.'MESSAGES CONTROL']
max-line-length = 88
disable = [
    "too-many-arguments",
    "invalid-name",
    "too-many-locals",
    "too-many-instance-attributes",
    "too-many-branches",
    "import-error",
    "too-many-try-statements",
    "compare-to-zero",
]

[tool.pylint.'TYPECHECK']
ignored-classes = "TextFileReader"

[tool.isort]
profile = "black"

[tool.black]
line-length = 88
skip-string-normalization = true
target-version = [
    'py38',
    'py39',
    'py310',
    'py311',
]

[tool.pytest.ini_options]
# pytest 3.10 has broken minversion checks,
# so we have to disable this until pytest 3.11
# minversion = 3.3
addopts = [
    "--verbose",
    "--color=yes",
    "--junitxml=build/reports/unit-tests.xml",
    "--cov-report=term",
    "--cov-report=xml:build/reports/code-coverage.xml",
    "--cov=nb_jeanzay_conda_kernels",
]
python_files = "test_*.py"
markers = [
    "group: mark as a test for groups",
    "services: mark as a services test",
    "user: mark as a test for a user",
    "slow: mark a test as slow",
]

[tool.coverage.run]
omit = [
    "tests/*",
]
parallel = true
branch = false

[tool.coverage.report]
# Regexes for lines to exclude from consideration
exclude_lines = [
    "if self.debug:",
    "pragma: no cover",
    "if __name__ == .__main__.:",
    "raise NotImplementedError",
]
ignore_errors = true
omit = [
    "tests/*",
    "nb_jeanzay_conda_kernels/_version.py",
    "*/site-packages/*"
]
